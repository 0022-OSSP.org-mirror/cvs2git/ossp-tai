dnl ##
dnl ##  OSSP tai - Time Handling
dnl ##  Copyright (c) 2002-2003 Ralf S. Engelschall <rse@engelschall.com>
dnl ##  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
dnl ##  Copyright (c) 2002-2003 Cable & Wireless Deutschland <http://www.cw.com/de/>
dnl ##
dnl ##  This file is part of OSSP tai, a time handling library
dnl ##  which can be found at http://www.ossp.org/pkg/lib/tai/.
dnl ##
dnl ##  Permission to use, copy, modify, and distribute this software for
dnl ##  any purpose with or without fee is hereby granted, provided that
dnl ##  the above copyright notice and this permission notice appear in all
dnl ##  copies.
dnl ##
dnl ##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
dnl ##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
dnl ##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
dnl ##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
dnl ##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
dnl ##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
dnl ##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
dnl ##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
dnl ##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
dnl ##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
dnl ##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
dnl ##  SUCH DAMAGE.
dnl ##
dnl ##  tm.ac: OSSP tm Autoconf checks
dnl ##

dnl #   Check for anything OSSP tai wants to know
dnl #   configure.in:
dnl #     TAI_CHECK_ALL

AC_DEFUN(TAI_CHECK_ALL,[
    #   gmtime_r hack for Solaris (FIXME)
    CFLAGS="$CFLAGS -D_REENTRANT"

    #   check for system functions
    AC_CHECK_FUNCS(gmtime_r timegm)

    #   determine whether "tm_gmtoff" exists in "struct tm"
    AC_CHECK_MEMBER(struct tm.tm_gmtoff, 
                    AC_DEFINE(HAVE_TM_GMTOFF, 1, [Define to 1 if "struct tm" has "tm_gmtoff"]),,
                    [#include <time.h>])
])

