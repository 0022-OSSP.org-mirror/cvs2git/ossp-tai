##
##  OSSP tai - Time Handling
##  Copyright (c) 2002-2003 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2002-2003 Cable & Wireless Deutschland <http://www.cw.com/de/>
##
##  This file is part of OSSP tai, a time handling library
##  which can be found at http://www.ossp.org/pkg/lib/tai/.
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  Makefile.in: make(1) build procedure
##

@SET_MAKE@

DESTDIR     =
prefix      = @prefix@
exec_prefix = @exec_prefix@
bindir      = @bindir@
libdir      = @libdir@
includedir  = @includedir@
mandir      = @mandir@

CC          = @CC@
CPPFLAGS    = @CPPFLAGS@
CFLAGS      = @DEFS@ @CFLAGS@
LDFLAGS     = @LDFLAGS@
LIBS        = @LIBS@
RM          = rm -f
RMDIR       = rmdir
SHTOOL      = ./shtool
LIBTOOL     = ./libtool
TRUE        = true

LIB_NAME    = libtai.la
LIB_OBJS    = tai_lib.lo tai_format.lo tai_parse.lo tai_data.lo tai_cal.lo tai_ui64.lo

TST_NAME    = tai_test
TST_OBJS    = tai_test.o ts.o

.SUFFIXES:
.SUFFIXES: .c .o .lo

all: $(LIB_NAME) $(TST_NAME)

.c.o:
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $<

.c.lo:
	@$(LIBTOOL) --mode=compile $(CC) $(CPPFLAGS) $(CFLAGS) -c $<

$(LIB_NAME): $(LIB_OBJS)
	@$(LIBTOOL) --mode=link $(CC) -o $(LIB_NAME) $(LIB_OBJS) -rpath $(libdir) \
	    -version-info `$(SHTOOL) version -l txt -d libtool VERSION`

$(TST_NAME): $(TST_OBJS) $(LIB_NAME)
	@$(LIBTOOL) --mode=link $(CC) $(LDFLAGS) -o $(TST_NAME) $(TST_OBJS) $(LIB_NAME) $(LIBS)

check: $(TST_NAME)
	@$(LIBTOOL) --mode=execute ./$(TST_NAME)

install:
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(prefix)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(bindir)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(includedir)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(libdir)
	$(SHTOOL) install -c -m 755 tai-config $(DESTDIR)$(bindir)/
	$(SHTOOL) install -c -m 644 tai.h $(DESTDIR)$(includedir)/
	@$(LIBTOOL) --mode=install $(SHTOOL) install -c -m 644 libtai.la $(DESTDIR)$(libdir)/

uninstall:
	@$(LIBTOOL) --mode=uninstall $(RM) $(DESTDIR)$(libdir)/libtai.la
	-$(RM) $(DESTDIR)$(includedir)/tai.h
	-$(RM) $(DESTDIR)$(bindir)/tai-config
	-$(RMDIR) $(DESTDIR)$(libdir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(includedir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(bindir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(prefix) >/dev/null 2>&1 || $(TRUE)

clean:
	-$(RM) $(LIB_NAME) $(LIB_OBJS)
	-$(RM) $(TST_NAME) $(TST_OBJS)
	-$(RM) -r .libs >/dev/null 2>&1 || $(TRUE)
	-$(RM) *.o *.lo

distclean: clean
	-$(RM) config.log config.status config.cache
	-$(RM) Makefile config.h tai-config
	-$(RM) libtool

realclean: distclean
	-$(RM) configure config.h.in
	-$(RM) shtool
	-$(RM) ltmain.sh libtool.m4 config.guess config.sub

