/*
**  OSSP tai - Time Handling
**  Copyright (c) 2002-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2003 Cable & Wireless Deutschland <http://www.cw.com/de/>
**
**  This file is part of OSSP tai, a time handling library
**  which can be found at http://www.ossp.org/pkg/lib/tai/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  tai.h: public API
*/

#ifndef __TAI_H__
#define __TAI_H__

#include <time.h> /* time_t, struct tm */

typedef enum {
    TAI_OK = 0, /* also used for TRUE */
    TAI_FALSE,
    TAI_ERR_ARG,
    TAI_ERR_USE,
    TAI_ERR_OVF,
    TAI_ERR_FMT,
    TAI_ERR_PRS,
    TAI_ERR_SYS,
    TAI_ERR_IMP
} tai_rc_t;

typedef struct {
    int           sign;
    unsigned long  sec;
    unsigned long nsec;
    unsigned long asec;
} tai_offset_t;

typedef struct {
    char bytes[16];
} tai_packed_t;

typedef enum {
    /* lossless types */
    TAI_TYPE_TMT,       /* tai_t */
    TAI_TYPE_PACKED,    /* tai_packed_t (128 bit in 16 bytes network byte order) */
    /* looser types */
    TAI_TYPE_OFFSET,    /* tai_off_t */
    TAI_TYPE_SECONDS,   /* unsigned long */
    TAI_TYPE_NSECONDS,  /* unsigned long */
    TAI_TYPE_ASECONDS,  /* unsigned long */
    TAI_TYPE_TIMET,     /* time_t */
    TAI_TYPE_STRUCTTM,  /* struct tm */
    TAI_TYPE_STRUCTTV,  /* struct timeval */
    /* only import */
    TAI_TYPE_UNIX,      /* gettimeofday/time/getthtime */
    TAI_TYPE_SNTP,      /* SNTP network query */
    TAI_TYPE_RTIME      /* time network query */
} tai_type_t;

typedef enum {
    /* arithmetical operations */
    TAI_OP_NEG,
    TAI_OP_ADD,
    TAI_OP_SUB,
    TAI_OP_MUL,
    TAI_OP_DIV,
    TAI_OP_MOD,
    /* comparison operations */
    TAI_OP_G0,  /* greater than 0 */
    TAI_OP_E0,  /* equal to 0 */
    TAI_OP_L0,  /* less than 0 */
    TAI_OP_NE,
    TAI_OP_EQ,
    TAI_OP_LT,
    TAI_OP_LE,
    TAI_OP_GT,
    TAI_OP_GE,
    TAI_OP_SETPREC,
    TAI_OP_GETPREC
} tai_op_t;

struct tai_st;
typedef struct tai_st tai_t;

tai_rc_t  tai_create  (tai_t **tai);
tai_rc_t  tai_destroy (tai_t  *tai);

tai_rc_t  tai_import  (tai_t  *tai, tai_type_t type, ...);
tai_rc_t  tai_export  (tai_t  *tai, tai_type_t type, ...);

tai_rc_t  tai_format  (tai_t  *tai, char       *buf_ptr, size_t buf_len, const char *fmt);
tai_rc_t  tai_parse   (tai_t  *tai, const char *buf_ptr, size_t buf_len, const char *fmt);

tai_rc_t  tai_op      (tai_t  *tai, tai_op_t op, ...);

#endif /* __TAI_H__ */

