/*
**  OSSP tai - Time Handling
**  Copyright (c) 2002 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002 Cable & Wireless Deutschland <http://www.cw.com/de/>
**
**  This file is part of OSSP tai, a time handling library
**  which can be found at http://www.ossp.org/pkg/lib/tai/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  tai_cal.h: calendar calculations (header)
*/

#ifndef __TAI_CAL_H__
#define __TAI_CAL_H__

#define TAI_CAL_SUNDAY            0
#define TAI_CAL_MONDAY            1
#define TAI_CAL_TUESDAY           2
#define TAI_CAL_WEDNESDAY         3
#define TAI_CAL_THURSDAY          4
#define TAI_CAL_FRIDAY            5
#define TAI_CAL_SATURDAY          6

#define TAI_CAL_JANUARY           0
#define TAI_CAL_FEBRUARY          1
#define TAI_CAL_MARCH             2
#define TAI_CAL_APRIL             3
#define TAI_CAL_MAY               4
#define TAI_CAL_JUNE              5
#define TAI_CAL_JULY              6
#define TAI_CAL_AUGUST            7
#define TAI_CAL_SEPTEMBER         8
#define TAI_CAL_OCTOBER           9
#define TAI_CAL_NOVEMBER          10
#define TAI_CAL_DECEMBER          11

#define TAI_CAL_YEAR_BASE         1900

typedef struct {
    int    sec;    /* seconds after the minute [0-61] */
    int    min;    /* minutes after the hour [0-59] */
    int    hour;   /* hours since midnight [0-23] */
    int    mday;   /* day of the month [1-31] */
    int    mon;    /* months since January [0-11] */
    int    year;   /* years since 1900 */
    int    wday;   /* days since Sunday [0-6] */
    int    yday;   /* days since January 1 [0-365] */
    long   offs;   /* offset from UTC+0 in seconds */
    int    isdst;  /* Daylight Savings Time flag */
} tai_cal_t;

#endif /* __TAI_CAL_H__ */

