/*-
 * Copyright (c) 1997-2002 FreeBSD Project.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $FreeBSD: src/lib/libc/stdtime/timelocal.h,v 1.11 2002/01/24 15:07:44 phantom Exp $
 */

#ifndef _TIMELOCAL_H_
#define	_TIMELOCAL_H_

#include <time.h>

#include "config.h"
#include "tai.h"
#include "tai_ui64.h"
#include "tai_cal.h"

struct tai_st {
    tai_ui64_t sec;    /* seconds (integral part) */
    tai_ui64_t asec;   /* atto-seconds (fractional part) */
    /* FIXME: obsolete stuff */
    int    tai_sec;    /* seconds after the minute [0-61] */
    int    tai_min;    /* minutes after the hour [0-59] */
    int    tai_hour;   /* hours since midnight [0-23] */
    int    tai_mday;   /* day of the month [1-31] */
    int    tai_mon;    /* months since January [0-11] */
    int    tai_year;   /* years since 1900 */
    int    tai_wday;   /* days since Sunday [0-6] */
    int    tai_yday;   /* days since January 1 [0-365] */
    int    tai_isdst;  /* Daylight Savings Time flag */
    long   tai_gmtoff; /* offset from CUT in seconds */
};

/*
 * Private header file for the strftime and strptime localization
 * stuff.
 */
typedef struct {
	const char	*mon[12];
	const char	*month[12];
	const char	*wday[7];
	const char	*weekday[7];
	const char	*X_fmt;
	const char	*x_fmt;
	const char	*c_fmt;
	const char	*am;
	const char	*pm;
	const char	*date_fmt;
	const char	*alt_month[12];
	const char	*md_order;
	const char	*ampm_fmt;
} tai_locale_t;

extern const tai_locale_t tai_locale;

size_t  tai_format_int(char *const s, const size_t maxsize, const char *const format, const struct tm *const t);
char   *tai_parse_int (const char *buf, const char *fmt, struct tm *tm);

tai_rc_t tai_tai2cal(const tai_t *tai,       tai_cal_t *cal, long  offset, int  isdst);
tai_rc_t tai_cal2tai(      tai_t *tai, const tai_cal_t *cal, long *offset, int *isdst);

extern ui64_t tai_full_sec_in_asec;
extern ui64_t tai_half_sec_in_asec;

#endif /* !_TIMELOCAL_H_ */
