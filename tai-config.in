#!/bin/sh
##
##  OSSP tai - Time Handling
##  Copyright (c) 2002-2003 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2002-2003 Cable & Wireless Deutschland <http://www.cw.com/de/>
##
##  This file is part of OSSP tai, a time handling library
##  which can be found at http://www.ossp.org/pkg/lib/tai/.
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  tm-config.in: OSSP tm build utility
##

DIFS=' 	
'

prefix="@prefix@"
exec_prefix="@exec_prefix@"

tai_prefix="$prefix"
tai_exec_prefix="$exec_prefix"
tai_bindir="@bindir@"
tai_libdir="@libdir@"
tai_includedir="@includedir@"
tai_mandir="@mandir@"
tai_datadir="@datadir@"
tai_acdir="@datadir@/aclocal"
tai_cflags="@CFLAGS@"
tai_ldflags="@LDFLAGS@"
tai_libs="@LIBS@"
tai_version="@TM_VERSION_STR@"

help=no
version=no

usage="tm-config"
usage="$usage [--help] [--version] [--all]"
usage="$usage [--prefix] [--exec-prefix] [--bindir] [--libdir] [--includedir] [--mandir] [--datadir] [--acdir]"
usage="$usage [--cflags] [--ldflags] [--libs]"
if [ $# -eq 0 ]; then
    echo "tm-config:Error: Invalid option" 1>&2
    echo "tm-config:Usage: $usage" 1>&2
    exit 1
fi
output=''
output_extra=''
all=no
prev=''
OIFS="$IFS" IFS="$DIFS"
for option
do
    if [ ".$prev" != . ]; then
        eval "$prev=\$option"
        prev=''
        continue
    fi
    case "$option" in
        -*=*) optarg=`echo "$option" | sed 's/[-_a-zA-Z0-9]*=//'` ;;
           *) optarg='' ;;
    esac
    case "$option" in
        --help|-h)
            echo "Usage: $usage"
            exit 0
            ;;
        --version|-v)
            echo "OSSP tm $tai_version"
            exit 0
            ;;
        --all)
            all=yes
            ;;
        --prefix)
            output="$output $tai_prefix"
            ;;
        --exec-prefix)
            output="$output $tai_exec_prefix"
            ;;
        --bindir)
            output="$output $tai_bindir"
            ;;
        --libdir)
            output="$output $tai_libdir"
            ;;
        --includedir)
            output="$output $tai_includedir"
            ;;
        --mandir)
            output="$output $tai_mandir"
            ;;
        --datadir)
            output="$output $tai_datadir"
            ;;
        --acdir)
            output="$output $tai_acdir"
            ;;
        --cflags)
            output="$output -I$tai_includedir"
            output_extra="$output_extra $tai_cflags"
            ;;
        --ldflags)
            output="$output -L$tai_libdir"
            output_extra="$output_extra $tai_ldflags"
            ;;
        --libs)
            output="$output -ltai"
            output_extra="$output_extra $tai_libs"
            ;;
        * )
            echo "tai-config:Error: Invalid option" 1>&2
            echo "tai-config:Usage: $usage" 1>&2
            exit 1;
            ;;
    esac
done
IFS="$OIFS"
if [ ".$prev" != . ]; then
    echo "tai-config:Error: missing argument to --`echo $prev | sed 's/_/-/g'`" 1>&2
    exit 1
fi
if [ ".$output" != . ]; then
    if [ ".$all" = .yes ]; then
        output="$output $output_extra"
    fi
    echo $output
fi

